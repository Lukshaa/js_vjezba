function loadData(url, callback){
  var xhttp = new XMLHttpRequest();

  xhttp.onreadystatechange = function(){
    if(this.readyState === 4 && this.status === 200){
      if(callback) callback(JSON.parse(xhttp.response));
    }
  }
  xhttp.open("GET", url, true);
  xhttp.send();
}

function parseData(data){
  console.log(data);
   console.log("data");
  topThree(data);
  longestName(data);
  percentOfBreweries(data);
  peti(data);
  abv(data);
  var sorted = sortByAbv(data);
  console.log(sorted);
  logRecursivly(sorted, ()=>{console.log("LogRecursivly done.");}, 0);
}

loadData('https://output.jsbin.com/nuredukoja.json', parseData);

function topThree(data){
  var countries = {};
  for(var i of data){
    if(!countries[i.country]){
        countries[i.country] = 0;
      }
      countries[i.country]++;
  }
  var sorted = Object.keys(countries).sort(function(a,b){return countries[b] - countries[a]});
  for(var i = 0; i < 3; i++){
    console.log("Country no." + i + " is: " + sorted[i] + " and it has " + countries[sorted[i]] + " breweries.")
  }
}
function longestName(data){
  var longest = data[0];

  for(var i of data){
    if(i.name.length > longest.name.length){
      longest = i;
    }
  }
  console.log("Name of beer: " + longest.name + ", brewery: " + longest.brewery_name + ", country: " + longest.country);
}
function percentOfBreweries(data){
  var totalNumber = data.length;
  var countries = {};
  for(var i of data){
    if(!countries[i.country]){
        countries[i.country] = 0;
      }
      countries[i.country]++;
  }
  var percentage = {};
  for(var i in countries){
    percentage[i] = parseFloat((countries[i] / totalNumber) * 100).toFixed(2);
  }
  for(var i in percentage){
    console.log("Country: " + i + ", percentage: " + percentage[i]);
  }
}
function peti(data){
  console.log("a) Total number of data: " + data.length);
  console.log("b) " + JSON.stringify(Object.keys(data[0])));
  console.log("c) " + data[parseInt(data.length/2)].name);
  for(var i of data){
    if(i.name.indexOf("Dragon") !== -1)
      console.log(i.name);
  }
}
function abv(data){
  var beers = {};
  var lightBeers = [], regularBeers = [], strongBeers = [];
  for(var i of data){
    if(i.abv < 5) lightBeers.push(i);
    else if(i.abv >= 5 && i.abv < 7) regularBeers.push(i);
    else strongBeers.push(i);
  }
  console.log("LightBeers count: " + lightBeers.length);
  console.log("RegularBeers count: " + regularBeers.length);
  console.log("StrongBeers count: " + strongBeers.length);
}
function sortByAbv(data){
  var sorted = data.sort(function(a,b) {return (a.abv < b.abv) ? 1 : ((a.abv > b.abv) ? -1 : 0);});
  return sorted;
}
function logRecursivly(data, callback, i){
  if(i === 10)callback();
  else{
    console.log(data[i].name);
    setTimeout(()=>{
      i++;
      logRecursivly(data, callback, i);
    }, 1000);
  }
}
