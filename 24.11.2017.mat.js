function loadData(url,callback){
var xhttp = new XMLHttpRequest();

xhttp.onreadystatechange = function() {
  if (this.readyState == 4 && this.status == 200) {
    if(callback) callback(JSON.parse(xhttp.response));
  }
};
xhttp.open("GET", url, true);
xhttp.send();
}

function parseData(data){
  console.log(data);
  sortById(data);
  sortBySongLength(data);
  checkGenre(data);
  checkDate(data);
  recursiveNames(data.songs, 0);
}

loadData("https://api.myjson.com/bins/19cbhb", parseData);
//1.
function sortById(data){
  var sorted = data.songs.sort((a,b)=>{
    return b.id > a.id;
  });
  for(var i of sorted){
    console.log(i.id + " - " + i.songName + " - " + data.artists[i.artist_id]);
  }
}
//2.
function sortBySongLength(data){
  var sorted = data.songs.sort((a,b)=>{
    var a_min = a.songLength.split(":")[0];
    var a_sec = a.songLength.split(":")[1];
    var b_min = b.songLength.split(":")[0];
    var b_sec = b.songLength.split(":")[1];
    return (b_min*60 + b_sec) > (a_min*60 + a_sec);
  });
  for(var i = 0; i<3; i++){
    console.log(sorted[i].songName);
  }
}
//3.
function checkGenre(data){
  var genre = [];
  data.songs.forEach((song)=>{
    for(var i of song.genre){
      genre.push(i);
    }
  });
  console.log("Most frequent genre is: " + mostFrequent(genre));
}
function mostFrequent(genre){
  var counts = {};
  var compare = 0;
  for(var i = 0; i < genre.length; i++){
       var g = genre[i];
       if(counts[g] === undefined){
           counts[g] = 1;
       }else{
           counts[g] = counts[g] + 1;
       }
       if(counts[g] > compare){
             compare = counts[g];
             mostFrequent = genre[i];
       }
    }
  return mostFrequent;
}
//4.
function checkDate(data){
  var arrayDay = [];
  data.songs.forEach((song)=>{
    var day = new Date(song.debut).getDay();
    console.log("Song '" + song.songName + "' was released on " + setDay(day));
    arrayDay.push(day);
  });
  moreThanOnce(arrayDay);
}

function setDay(day){
  switch (day) {
    case 0:
      return "Sunday";
      break;
    case 1:
      return "Monday";
      break;
    case 2:
      return "Tuesday";
      break;
    case 3:
      return "Wednesday";
      break;
    case 4:
      return "Thursday";
      break;
    case 5:
      return "Friday";
      break;
    case 6:
      return "Saturday";
      break;
  }
}

//5.
function moreThanOnce(array){
  var counts = [];
  var compare = 0;
  for(var i = 0; i < array.length; i++){
       var g = array[i];
       if(counts[g] === undefined){
           counts[g] = 1;
       }else{
           counts[g] = counts[g] + 1;
       }
    }
  for(var i=0; i<counts.length; i++){
    if(counts[i]>1){
      console.log(setDay(i) + " is repeated more than once. It's repeated " + counts[i] + " times.");
    }
  }
}

//6.
function recursiveNames(songs, i){
  if(songs[i] !== undefined){
    console.log(songs[i].songName);
    i +=1;
    recursiveNames(songs, i);
  }
}
