//Prvi Zadatak
function loadData(url, callback){
  var xhttp = new XMLHttpRequest();

  xhttp.onreadystatechange = function(){
    if(this.readyState === 4 && this.status === 200){
      if(callback) callback(JSON.parse(xhttp.response));
    }
  };

  xhttp.open("GET", url, true);
  xhttp.send();
}

function parseData(data){
  console.log(data);
  sortById(data, call);
  sortByTotalAmount(data, call);
  sumByDays(data);
  calculateTotals(data);
  logRecursively(data, logRecursivelyFinish, 0);
}

loadData("https://api.myjson.com/bins/8dpdh", parseData);
function call(){
console.log("It is over!");
}
//Drugi Zadatak
function sortById(data, callback){
  data.amounts.sort(function(a,b){
    return b.id < a.id;
  });
  for(var i of data.amounts){
    console.log(i.id + " - " + data.meta[i.id]);
  }
  callback();
}

//Treci Zadatak
function sortByTotalAmount(data, callback){
  data.amounts.sort((a,b)=>{
    return b.totalAmount - a.totalAmount;
  });
  for(var i=0; i<3; i++){
    console.log(data.meta[data.amounts[i].id] + " - " + data.amounts[i].totalAmount);
  }
  callback();
}
//Cetvrti Zadatak
function sumByDays(data){
  var days = {
    0:"Sunday",
    1:"Monday",
    2:"Tuesday",
    3:"Wednesday",
    4:"Thursday",
    5:"Friday",
    6:"Saturday"
  };
  var sum = {};
  for(var i of data.amounts){
    var day = new Date(i.datetime).getDay();
    if(!sum[day]) sum[day] = 0;
    sum[day] += parseFloat(i.totalAmount);
  }
  for(var i in days){
    if(sum[i] === undefined) sum[i] = 0;
    console.log(days[i] + " - " + sum[i]);
  }
}

//Peti Zadatak
function calculateTotals(data){
  var totalAmount = 0, totalPayout = 0, totalDiffTickets = [];
  for(var i of data.amounts){
    totalAmount += parseFloat(i.totalAmount);
    if(typeof i.totalPayout === 'number')
      totalPayout += i.totalPayout;
    for(var j in i.tickets){
      if(totalDiffTickets.indexOf(i.tickets[j]) === -1)
      totalDiffTickets.push(i.tickets[j]);
    }
  }
  console.log("Total amount: " + totalAmount);
  console.log("Total payouts: " + totalPayout);
  console.log("Total diff tickets: " + totalDiffTickets.length);
}
//Sesti Zadatak
function logRecursively(data, callback, i){
  if(i === data.amounts.length-1){
    callback();
  }
  else{
    console.log(data.meta[data.amounts[i].id] + " - " + JSON.stringify(data.amounts[i]));
    i++;
    setTimeout(logRecursively(data, callback, i), 1000);
  }
}
function logRecursivelyFinish(){
  console.log("Recursive log finished.");
}
